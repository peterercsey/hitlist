var app = angular.module('MyApp', ['appRoutes','mainCtrl', 'mainService', 'authService',  'userCtrl', 'userService','reverseDirective'])

.config(function($httpProvider){
	$httpProvider.interceptors.push('AuthInterceptor');
})

