var User = require('../models/user');


var config = require('../../config');
var secretKey = config.secretKey;

var jsonwebtoken = require('jsonwebtoken');

var mongoose = require('mongoose');

function createToken(user){

	var token = jsonwebtoken.sign({

		id: user._id,
		name: user.name,
		username: user.username,
		rights: user.rights

	}, secretKey, {
		expiresInMinute: 1440
	});

	return token;
}

module.exports = function(app, express, io) {


	var api = express.Router();

	api.post('/signup',function(req, res){

		var user = new User({

			name : req.body.name,
			username : req.body.username,
			password: req.body.password,
			rights : 1

		});

		var token = createToken(user);

		user.save(function(err){
			if(err){
				res.send(err);
				return;
			}

			res.json({ success:true, message: 'User has been created!', token:token})
		});
	});


	api.get('/users',function(req, res){

		User.find({}, function(err, users){
			if(err){
				res.send(err);
				return;
			}

			res.json(users);

		});
	});



	api.post('/login', function(req, res){

		User.findOne({ 
			username : req.body.username
		}).select('name username password rights').exec(function(err, user) {

			if(err) throw err;

			if(!user){
				res.send({ message: "User doesn't exist"});
			} else if(user){

				var validPassword = user.comparePassword(req.body.password);

				if(!validPassword){

					res.send({ message: "Invalid password"});
				} else{

					console.log(user);
					var token = createToken(user);
					res.json({
						success: true,
						message: "Successfully login",
						token: token
					});
				}

			}

		})

	});

	api.use(function(req, res, next){


		console.log('Somebody just came to our app');

		var token = req.body.token || req.param('token') || req.headers['x-access-token'];

			// check if token exist
			if(token ){

				jsonwebtoken.verify(token, secretKey, function(err, decoded){
					if(err){
						res.status(403).send({ success: false, message: "Failed to authenticate user"});
					}
					else{

						req.decoded = decoded;
						next();

					}

				});

			}
			else{


				res.status(403).send({ success:false, message: "No token provided"});

			}

		});

	// Destination B // provide a legitimate token

	// LOGGEN IN CONTENT

	api.route('/')

	// Uploading news data

	

	


api.get('/me', function (req, res){
	res.json(req.decoded);
});



return api;

}